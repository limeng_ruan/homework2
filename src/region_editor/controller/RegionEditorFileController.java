package region_editor.controller;

import java.io.File;
import world_data.Region;
import javafx.stage.FileChooser;
import static region_editor.RegionEditorConstants.APP_NAME;
import static region_editor.RegionEditorConstants.APP_NAME_FILE_NAME_SEPARATOR;
import static region_editor.RegionEditorConstants.MESSAGE_NO_FILE_SELECTED;
import static region_editor.RegionEditorConstants.MESSAGE_UNIQUE_ID;
import static region_editor.RegionEditorConstants.MESSAGE_WORLD_LOADED;
import static region_editor.RegionEditorConstants.MESSAGE_WORLD_LOADING_ERROR;
import static region_editor.RegionEditorConstants.MESSAGE_WORLD_SAVED;
import static region_editor.RegionEditorConstants.MESSAGE_WORLD_SAVING_ERROR;
import static region_editor.RegionEditorConstants.PATH_DATA;
import static region_editor.RegionEditorConstants.PROMPT_A_OVERWRITE_FILE_REQUEST;
import static region_editor.RegionEditorConstants.PROMPT_B_OVERWRITE_FILE_REQUEST;
import static region_editor.RegionEditorConstants.PROMPT_DELETE;
import static region_editor.RegionEditorConstants.PROMPT_ENTER_NEW_REGION_ID;

import static region_editor.RegionEditorConstants.PROMPT_SAVE;
import static region_editor.RegionEditorConstants.PROMPT_UPDATE;
import static region_editor.RegionEditorConstants.PROMPT_WORLD_NAME_REQUEST;
import static region_editor.RegionEditorConstants.TITLE_DELETE;
import static region_editor.RegionEditorConstants.TITLE_ENTER_NEW_REGION_ID;

import static region_editor.RegionEditorConstants.TITLE_OVERWRITE_FILE_REQUEST;
import static region_editor.RegionEditorConstants.TITLE_SAVE;
import static region_editor.RegionEditorConstants.TITLE_UPDATE;
import static region_editor.RegionEditorConstants.TITLE_WORLD_NAME_REQUEST;
import static region_editor.RegionEditorConstants.WORLD_FILE_EXTENSION;
import region_editor.view.ConfirmDialog;
import static region_editor.view.ConfirmDialog.CANCEL;
import static region_editor.view.ConfirmDialog.OK;
import static region_editor.view.ConfirmDialog.YES;
import region_editor.view.InputDialog;
import region_editor.view.MessageDialog;
import region_editor.view.RegionEditorView;
import world_data.WorldDataManager;

/**
 * This class provides all the file servicing for the application. This
 * means it directs all operations regarding creating new, opening, loading, and
 * saving files, Note that it employs use of WorldIO for the actual file work,
 * this class manages when to actually read and write from/to files, prompting
 * the user when necessary for file names and validation on actions.
 *
 * @author  Richard McKenna 
 *          Debugging Enterprises
 * @version 1.0
 */
public class RegionEditorFileController
{
    // THIS IS THE VIEW THAT THIS FILE MANAGER NEEDS TO USE
    private RegionEditorView view;

    // WE'LL STORE THE FILE CURRENTLY BEING WORKED ON
    // AND THE NAME OF THE FILE
    private File currentFile;
    private String currentFileName;
    
    // WE WANT TO KEEP TRACK OF WHEN SOMETHING HAS NOT BEEN SAVED
    private boolean saved;
    
    // THESE ARE OUR DIALOGS
    MessageDialog messageDialog;
    InputDialog inputDialog;
    ConfirmDialog confirmDialog;
    
    /**
     * This default constructor starts the program without a world file being
     * edited.
     * 
     * @param initView The view this file manager will be providing
     * file services to.
     */
    public RegionEditorFileController(RegionEditorView initView)
    {
        // KEEP THE APP FOR LATER
        view = initView;

        // NOTHING YET
        currentFile = null;
        currentFileName = null;
        saved = true;
	
	// INIT THE DIALOGS
	messageDialog = new MessageDialog(view.getWindow());
	inputDialog = new InputDialog(view.getWindow(), messageDialog);
	confirmDialog = new ConfirmDialog(view.getWindow());
    }

    /**
     * This method starts the process of editing a new world. If a world is
     * already being edited, it will prompt the user to save it first.
     */
    public void processNewWorldRequest()
    {
        // WE MAY HAVE TO SAVE CURRENT WORK
        boolean continueToMakeNew = true;
        if (!saved)
        {
            // THE USER CAN OPT OUT HERE WITH A CANCEL
            continueToMakeNew = promptToSave();
        }

        // IF THE USER REALLY WANTS TO MAKE A NEW WORLD
        if (continueToMakeNew)
        {
            // GO AHEAD AND PROCEED MAKING A NEW WORLD
            continueToMakeNew = promptForNew(true);
            if (continueToMakeNew){
                view.enableSaveAsButton(true);
            }
        }
        
    }

    /**
     * This method lets the user open a world saved to a file. It will also make
     * sure data for the current world is not lost.
     */
    public void processOpenWorldRequest()
    {
        // WE MAY HAVE TO SAVE CURRENT WORK
        boolean continueToOpen = true;
        if (!saved)
        {
            // THE USER CAN OPT OUT HERE WITH A CANCEL
            continueToOpen = promptToSave();
        }

        // IF THE USER REALLY WANTS TO OPEN A WORLD
        if (continueToOpen)
        {
            // GO AHEAD AND PROCEED MAKING A NEW WORLD
            continueToOpen = promptToOpen();
            if (continueToOpen)
            {
                view.enableSaveAsButton(true);
            }
        }
    }

    /**
     * This method will save the current world to a file. Note that we already
     * know the name of the file, so we won't need to prompt the user.
     */
    public void processSaveWorldRequest()
    {
        // GET HTE DATA MANAGER
        WorldDataManager dataManager = view.getWorldDataManager();

        // DON'T ASK, JUST SAVE
        boolean savedSuccessfully = dataManager.save(currentFile);
        if (savedSuccessfully)
        {
            // MARK IT AS SAVED
            saved = true;

            // AND REFRESH THE GUI
            view.enableSaveButton();
        }
    }

    /**
     * This method will save the current world as a named file provided by the
     * user.
     */
    public void processSaveAsWorldRequest()
    {
        // ASK THE USER FOR A FILE NAME
        promptForNew(false);
    }

    /**
     * This method will exit the application, making sure the user doesn't lose
     * any data first.
     */
    public void processExitRequest()
    {
        // WE MAY HAVE TO SAVE CURRENT WORK
        boolean continueToExit = true;
        if (!saved)
        {
            // THE USER CAN OPT OUT HERE
            continueToExit = promptToSave();
        }

        // IF THE USER REALLY WANTS TO EXIT THE APP
        if (continueToExit)
        {
            // EXIT THE APPLICATION
            System.exit(0);
        }
    }
    
    /**
     * This method prompts the user to choose if they really want to change the information of selected Region
     * @return true if they select OK.
     * co-author: Limeng Ruan
     */
    public boolean promptForUpdate(){
        String selection=confirmDialog.showOkCancel(TITLE_UPDATE, PROMPT_UPDATE);
        return selection.equals(OK);
    }
    
    /**
     * This method prompts the user to confirm the delete.
     * @return true if user selects "OK".
     * co-author: Limeng Ruan
     */
    public boolean promptForDelete(){
        String selection=confirmDialog.showOkCancel(TITLE_DELETE, PROMPT_DELETE); 
        return selection.equals(OK);
    }
    
    /**
     * This method asks the user the id of the region.
     * @return The id user enters.
     * co-author: Limeng Ruan
     */
    public String promptForAdd(){
       
        String regionID=inputDialog.showInputDialog(TITLE_ENTER_NEW_REGION_ID, PROMPT_ENTER_NEW_REGION_ID);
        return regionID;
    }
    

    /**
     * This helper method asks the user for a name for the world about to be
     * created. Note that when the world is created, a corresponding .xml file
     * is also created.
     *
     * @return true if the user goes ahead and provides a good name false if
     * they cancel.
     * co-author: Limeng Ruan
     */
    private boolean promptForNew(boolean brandNew)
    {
        // SO NOW ASK THE USER FOR A WORLD NAME
        String worldName = inputDialog.showInputDialog(TITLE_WORLD_NAME_REQUEST, PROMPT_WORLD_NAME_REQUEST);

        // GET THE WORLD DATA
        WorldDataManager dataManager = view.getWorldDataManager();

        // IF THE USER CANCELLED, THEN WE'LL GET A fileName
        // OF NULL, SO LET'S MAKE SURE THE USER REALLY
        // WANTS TO DO THIS ACTION BEFORE MOVING ON
        if ((worldName != null)
                && (worldName.length() > 0))
        { 
            // WE ARE STILL IN DANGER OF AN ERROR DURING THE WRITING
            // OF THE INITIAL FILE, SO WE'LL NOT FINALIZE ANYTHING
            // UNTIL WE KNOW WE CAN WRITE TO THE FILE
            String fileNameToTry = worldName + WORLD_FILE_EXTENSION;
            File fileToTry = new File(PATH_DATA + fileNameToTry);
            if (fileToTry.isDirectory())
            {
                return false;
            }
            String selection = CANCEL;
            if (fileToTry.exists())
            {
                selection = confirmDialog.showOkCancel(
			TITLE_OVERWRITE_FILE_REQUEST, 
                        PROMPT_A_OVERWRITE_FILE_REQUEST + fileNameToTry + PROMPT_B_OVERWRITE_FILE_REQUEST);
            }
            if (selection.equals(OK)||(!fileToTry.exists()))
            {
                // MAKE OUR NEW WORLD
                if (brandNew)
                {
                    dataManager.reset(worldName);
                }
                
                // INITIALIZE THE GUI CONTROLS IF THIS IS
                // THE FIRST WORLD THIS SESSION
                view.initRegionControls();
        
		// NOW FOR THE DATA. THIS RELOADS ALL REGIONS INTO THE TREE
		view.initWorldTree();

                // NOW SAVE OUR NEW WORLD
                dataManager.save(fileToTry);
            
                // NO ERROR, SO WE'RE HAPPY
                saved = true;

                // UPDATE THE FILE NAMES AND FILE
                currentFileName = fileNameToTry;
                currentFile = fileToTry;

                // SELECT THE ROOT NODE, WHICH SHOULD FORCE A
                // TRANSITION INTO THE REGION VIEWING STATE
                // AND PUT THE FILE NAME IN THE TITLE BAR
                view.getWindow().setTitle(APP_NAME + APP_NAME_FILE_NAME_SEPARATOR + currentFileName);
                
                // WE DID IT!
                return true;
            }
        }
        // USER DECIDED AGAINST IT
        return false;
    }

    /**
     * This helper method verifies that the user really wants to save their
     * unsaved work, which they might not want to do. Note that it could be used
     * in multiple contexts before doing other actions, like creating a new
     * world, or opening another world, or exiting. Note that the user will be
     * presented with 3 options: YES, NO, and CANCEL. YES means the user wants
     * to save their work and continue the other action (we return true to
     * denote this), NO means don't save the work but continue with the other
     * action (true is returned), CANCEL means don't save the work and don't
     * continue with the other action (false is returned).
     *
     * @return true if the user presses the YES option to save, true if the user
     * presses the NO option to not save, false if the user presses the CANCEL
     * option to not continue.
     * co-author: Limeng Ruan
     */
    private boolean promptToSave()
    {
        // PROMPT THE USER TO SAVE UNSAVED WORK
	String selection = CANCEL;
	selection = confirmDialog.showYesNoCancel(TITLE_SAVE, PROMPT_SAVE);

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (selection.equals(YES))
        {
            WorldDataManager dataManager = view.getWorldDataManager();
            boolean saveSucceeded = dataManager.save(currentFile);
            if (saveSucceeded)
            {
		messageDialog.show(MESSAGE_WORLD_SAVED);
                saved = true;
            } else
            {
                // SOMETHING WENT WRONG WRITING THE XML FILE
		messageDialog.show(MESSAGE_WORLD_SAVING_ERROR);
            }
        } // IF THE USER SAID CANCEL, THEN WE'LL TELL WHOEVER
        // CALLED THIS THAT THE USER IS NOT INTERESTED ANYMORE
        else if (selection.equals(CANCEL))
        {
            return false;
        }

        // IF THE USER SAID NO, WE JUST GO ON WITHOUT SAVING
        // BUT FOR BOTH YES AND NO WE DO WHATEVER THE USER
        // HAD IN MIND IN THE FIRST PLACE
        return true;
    }

    /**
     * This helper method asks the user for a file to open. The user-selected
     * file is then loaded and the GUI updated. Note that if the user cancels
     * the open process, nothing is done. If an error occurs loading the file, a
     * message is displayed, but nothing changes.
     */
    private boolean promptToOpen()
    {
        // ASK THE USER FOR THE WORLD TO OPEN
        FileChooser worldFileChooser = new FileChooser();
	worldFileChooser.setInitialDirectory(new File(PATH_DATA));
	File testFile = worldFileChooser.showOpenDialog(view.getWindow());

	if (testFile == null)            {
	    // TELL THE USER ABOUT THE ERROR
	    messageDialog.show(MESSAGE_NO_FILE_SELECTED);
	    return false;
	}
	
	// AND LOAD THE WORLD (XML FORMAT) FILE
	WorldDataManager dataManager = view.getWorldDataManager();
	boolean loadedSuccessfully = dataManager.load(testFile);
	if (loadedSuccessfully) {
	    view.initRegionControls();  
	    
	    // NOW FOR THE DATA. THIS RELOADS ALL REGIONS INTO THE TREE
	    view.initWorldTree();
	    currentFile = testFile;
	    currentFileName = currentFile.getName();
	    saved = true;
	    
	    // AND PUT THE FILE NAME IN THE TITLE BAR
	    view.getWindow().setTitle(APP_NAME + APP_NAME_FILE_NAME_SEPARATOR + currentFileName);
	    messageDialog.show(MESSAGE_WORLD_LOADED);
	    return true;
	} 
	else {
	    // TELL THE USER ABOUT THE ERROR
	    messageDialog.show(MESSAGE_WORLD_LOADING_ERROR);
	    return false;
	}
    }

    /**
     * This mutator method marks the file as not saved, which means that when
     * the user wants to do a file-type operation, we should prompt the user to
     * save current work first. Note that this method should be called any time
     * the world is changed in some way.
     */
    public void markFileAsNotSaved()
    {
        saved = false;
    }

    /**
     * Accessor method for checking to see if the current world has been saved
     * since it was last editing. If the current file matches the world data,
     * we'll return true, otherwise false.
     *
     * @return true if the current world is saved to the file, false otherwise.
     */
    public boolean isSaved()
    {
        return saved;
    }
    
    public void uniqueIdAlert(){
        messageDialog.show(MESSAGE_UNIQUE_ID);
    }
}