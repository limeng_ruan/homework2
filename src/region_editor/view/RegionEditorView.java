package region_editor.view;

import java.util.Collections;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.TreeMap;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import region_editor.controller.RegionEditorFileController;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import static region_editor.RegionEditorConstants.*;
import world_data.Region;
import world_data.RegionType;
import world_data.WorldDataManager;

/**
 *  This class manages the view for 
 * 
 *  @author Richard McKenna
 */
public class RegionEditorView{
    // HERE'S OUR DATA
    private WorldDataManager worldDataManager;

    // THIS WILL MANAGE RESPONSES TO THE FILE TOOLBAR
    private RegionEditorFileController fileController;
    
    // NOW FOR THE GUI

    // HERE'S OUR APP'S WINDOW
    private Stage window;
    
    // THIS IS THE WINDOW'S SCENE
    private Scene windowScene;
    
    // THIS WILL ORGANIZE ALL OF OUR STUFF
    private BorderPane windowPane;

    // THIS WILL CONTAIN THE TOOLBARS AND WILL GO IN THE NORTH
    private HBox topPane;

    // HERE'S A TOOLBAR TO GO IN THE NORTH WITH FILE CONTROLS
    private HBox  fileToolbar;
    private Button newButton;
    private Button openButton;
    private Button saveButton;
    private Button saveAsButton;
    private Button exitButton;

    // THESE ARE FOR ADDING, REMOVING, AND EDITING REGIONS
    private HBox regionEditorToolbar;
    private Button addRegionButton;
    private Button removeRegionButton;
    private Button editRegionButton;

    // THIS WILL SEPARATE THE CENTER OF OUR GUI INTO TWO HALVES (LEFT & RIGHT)
    private SplitPane splitPane;
    
    // HERE'S WHERE WE'LL PRESENT THE HIERARCHICAL 
    // REPRESENTATION OF OUR DATA
    private ScrollPane		worldTreeScrollPane;
    private TreeView<Region>	worldTree;
    private TreeItem<Region>    worldTreeRoot;

    // HERE WE'LL DISPLAY AND EDIT REGION INFO
    private GridPane	regionEditorPane;
    private Label	headerLabel;
    private Label	idLabel;
    private TextField	idTextField;
    private Label	nameLabel;
    private TextField	nameTextField;
    private Label	typeLabel;
    private ComboBox<RegionType> typeComboBox;
    private Label	capitalLabel;
    private TextField	capitalTextField;
    private HBox	updatePane;
    private Button	updateRegionButton;
    private Button	cancelUpdateButton;
    
    /**
     * Called at startup, this method initializes all GUI file controls, laying out
     * the necessary components and hooking up their event handlers, and then
     * opens the application window, sending it into event handling mode. Note
     * that the tree and region editing file controls will only be added to the
     * application once the first world is either created or loaded. Note that
     * when complete, the GUI is constructed and the window is opened and visible.
     */
    public RegionEditorView(Stage initWindow, WorldDataManager initWorldDataManager) {
	// KEEP THESE FOR LATER
	window = initWindow;
	worldDataManager = initWorldDataManager;
	
	// AND NOW INITIALIZE EVERYTHING ELSE
	
        // FIRST MAKE ALL THE GUI COMPONENTS
        initGUI();	

        // THEN CONNECT EVENT RESPONSES
        initControllers();	
	
        // AND THEN SETUP THE WINDOW
        initWindow();

        // AND GET THE APP ROLLING, OPENING THE WINDOW AND
        // MOVING AHEAD INTO EVENT HANDLING MODE
        window.show();	
    }

    // ACCESSOR METHODS
    
    /**
     * Accessor method for getting the application's top-level window,
     * within which all GUI components will be contained.
     * 
     * @return The top-level window of the application.
     */
    public Stage getWindow()
    {
        return window;
    }
    
    /**
     * Accessor method for getting the world tree.
     * 
     * @return The tree used to display the regions.
     */
    public TreeView getWorldTree() {
	return worldTree;
    }

    /**
     * Accessor method for getting the application's world data manager,
     * which manages all the geographic information, but has no knowledge
     * of its presentation in this application.
     * 
     * @return The application's geographic data manager.
     */
    public WorldDataManager getWorldDataManager()
    {
        return worldDataManager;
    }

    /**
     * Accessor method for getting the application's file manager, which
     * manages all reading and writing to and from files, as well as the
     * creation of new files.
     * 
     * @return The application's file manager.
     */
    public RegionEditorFileController getFileController()
    {
        return fileController;
    }
  
    /**
     * Accessor method for getting the text inside the id text field,
     * which the user may or may not have changed.
     * 
     * @return The text found inside the id text field.
     */
    public String getInputId()
    {
        return idTextField.getText();
    }
    
    /**
     * Accessor method for getting the text inside the name text field,
     * which the user may or may not have changed.
     * 
     * @return The text found inside the name text field.
     */
    public String getInputName()
    {
        return nameTextField.getText();
    }
    
    /**
     * Accessor method for getting the RegionType selected inside the 
     * type combo box, which the user may or may not have changed.
     * 
     * @return The RegionType found inside the type combo box.
     */    
    public RegionType getInputType()
    {
        return typeComboBox.getSelectionModel().getSelectedItem();
    }

    /**
     * Accessor method for getting the text inside the capital text field,
     * which the user may or may not have changed.
     * 
     * @return The text found inside the capital text field.
     */    
    public String getInputCapital()
    {
        return capitalTextField.getText();
    }

    // PRIVATE HELPER INIT METHODS

    /**
     * Called at startup, this helper method initializes all
     * the manager classes, which provide the app behaviour.
     * co-author: Limeng Ruan
     */
    private void initControllers()
    {
        // THE FILE CONTROLLER
        fileController = new RegionEditorFileController(this);
	newButton.setOnAction(e-> {
	    fileController.processNewWorldRequest();
	});
	openButton.setOnAction(e-> {
	    fileController.processOpenWorldRequest();
	});
	saveButton.setOnAction(e-> {
	    fileController.processSaveWorldRequest();
	});
	saveAsButton.setOnAction(e-> {
	    fileController.processSaveAsWorldRequest();
	});
	exitButton.setOnAction(e-> {
	    fileController.processExitRequest();
	});
        
       
        
        
     }
    
    

    /**
     * Called at startup, this method sets up the window for use. 
     */
    private void initWindow()
    {
	// SET THE TITLE BAR TEXT	
        window.setTitle(APP_NAME);
	
        // GET THE SIZE OF THE SCREEN
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
	
	// SIZE THE WINDOW AND POSITION IT SO IT'S CENTERED
        window.setWidth(WINDOW_WIDTH);
        window.setHeight(WINDOW_HEIGHT);
        window.setX((bounds.getWidth()/2) - (WINDOW_WIDTH/2));
        window.setY((bounds.getHeight()/2) - (WINDOW_HEIGHT/2));
	
        // SET THE APP ICON
        String appIcon = PROTOCOL_FILE + PATH_IMAGES + ICON_APP;
        window.getIcons().add(new Image(appIcon));

	// NOW CREATE THE WINDOW'S SCENE
        windowScene = new Scene(windowPane);

        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
        // WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
        windowScene.getStylesheets().add(STYLESHEET_PRIMARY);
        window.setScene(windowScene);
    }
    
    /**
     * Initializes all the GUI components excluding the world tree
     * and region editor controls.
     */
    private void initGUI() {
	// ULTIMATELY EVERYTHING WILL GO INTO THIS PANE, WHICH
	// WE'LL END UP PUTTING DIRECTLY IN THE WINDOW'S SCENE
	windowPane = new BorderPane();
	
        // WE'LL PUT ALL THE TOOLBARS IN THE TOP
        topPane = new HBox();
	windowPane.setTop(topPane);
        
        // THE FILE TOOLBAR CONTROLS
        fileToolbar = new HBox();
	fileToolbar.getStyleClass().add(CLASS_BORDERED_PANE);
        newButton       = initChildButton(fileToolbar,	ICON_NEW,	TOOLTIP_NEW,	    true);
        openButton      = initChildButton(fileToolbar,	ICON_OPEN,      TOOLTIP_OPEN,	    true);
        saveButton      = initChildButton(fileToolbar,	ICON_SAVE,      TOOLTIP_SAVE,	    false);
        saveAsButton    = initChildButton(fileToolbar,	ICON_SAVE_AS,   TOOLTIP_SAVE_AS,    false);
        exitButton      = initChildButton(fileToolbar,	ICON_EXIT,      TOOLTIP_EXIT,	    true);
        topPane.getChildren().add(fileToolbar);
	
	initRegionControls();
    }
    
    // INIT A BUTTON AND ADD IT TO A CONTAINER IN A TOOLBAR
    private Button initChildButton(Pane toolbar, String icon, String tooltip, boolean enabled) {
	Button button = initButton(icon, tooltip, enabled);
        toolbar.getChildren().add(button);
	return button;
    }
    
    private Button initButton(String icon, String tooltip, boolean enabled) {
        String imagePath = "file:" + PATH_IMAGES + icon;
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.setDisable(!enabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(tooltip);
        button.setTooltip(buttonTooltip);
        return button;	
    }
       
    // initRegionControls AND ITS HELPERS
     
    /**
     * Initializes the rest of the controls, including the world tree
     * and the region editing. Note that this is called after either a
     * new world is created or an existing one is loaded from a file.
     * co-author: Limeng Ruan
     */
    public void initRegionControls()
    {
        // ONLY DO THIS IF IT HASN'T BEEN DONE ALREADY
        if (addRegionButton != null)
        {
            return;
        }
        
        // WE'LL DISPLAY ALL THE REGIONS HERE
        initTree();        

       // THIS WILL HANDLE EVENTS FOR OUR REGION EDITOR BUTTONS
        regionEditorToolbar = new  HBox();
	regionEditorToolbar.getStyleClass().add(CLASS_BORDERED_PANE);
        addRegionButton     = initChildButton(regionEditorToolbar, ICON_ADD_REGION,    TOOLTIP_ADD_REGION,    true);
        removeRegionButton  = initChildButton(regionEditorToolbar, ICON_REMOVE_REGION, TOOLTIP_REMOVE_REGION, false);
        editRegionButton    = initChildButton(regionEditorToolbar, ICON_EDIT_REGION,   TOOLTIP_EDIT_REGION,   false);
        topPane.getChildren().add(regionEditorToolbar);

        // AND OUR REGION DISPLAY
        regionEditorPane = new GridPane();

        // INITIALIZE OUR HEADER
        headerLabel = new Label(HEADER_REGION_EDITOR);
        headerLabel.getStyleClass().add(CLASS_HEADING_LABEL);
        
        // NOW THE INPUT LABELS
        idLabel         = initPromptLabel(PROMPT_ID);
        nameLabel       = initPromptLabel(PROMPT_NAME);
        typeLabel       = initPromptLabel(PROMPT_TYPE);
        capitalLabel    = initPromptLabel(PROMPT_CAPITAL);

        // THE TEXT FIELDS
        idTextField         = initRegionEditorTextField();
        nameTextField       = initRegionEditorTextField();
        capitalTextField    = initRegionEditorTextField();
        
        // OUR COMBO BOX
	ObservableList<RegionType> comboModel = FXCollections.observableArrayList(RegionType.values());
        typeComboBox    = new ComboBox(comboModel);
	typeComboBox.getStyleClass().add(CLASS_REGION_COMBO);
        
        // AND THE UPDATE/CANCEL BUTTONS
        updateRegionButton = initButton(ICON_UPDATE, TOOLTIP_UPDATE_REGION,  true);
        cancelUpdateButton = initButton(ICON_CANCEL, TOOLTIP_CANCEL_UPDATE,  true);
        updatePane = new HBox();
        updatePane.getChildren().add(updateRegionButton);
        updatePane.getChildren().add(cancelUpdateButton);

        // NOW LAY EVERYTHING OUT
        regionEditorPane.add (headerLabel,        0, 0, 2, 1);
        regionEditorPane.add (idLabel,            0, 1, 1, 1);
        regionEditorPane.add (idTextField,        1, 1, 1, 1);
        regionEditorPane.add (nameLabel,          0, 2, 1, 1);
        regionEditorPane.add (nameTextField,      1, 2, 1, 1);
        regionEditorPane.add (typeLabel,          0, 3, 1, 1);
        regionEditorPane.add (typeComboBox,       1, 3, 1, 1);
        regionEditorPane.add (capitalLabel,       0, 4, 1, 1);
        regionEditorPane.add (capitalTextField,   1, 4, 1, 1);
        regionEditorPane.add (updatePane,         0, 5, 2, 1);    

        // AND FINALLY SETUP THE APP'S WORKBENCH, WHICH IS SPLIT
        // INTO TWO HALVES, THE WORLD TREE ON THE LEFT AND THE 
        // REGION VIEWING/EDITING ON THE RIGHT
        splitPane = new SplitPane();
	splitPane.getItems().addAll(worldTreeScrollPane, regionEditorPane);
        splitPane.setDividerPositions(SPLIT_PANE_LEFT_LOCATION);
        
         //Region editing controllers
        addRegionButton.setOnAction(e ->{
            addRegion(fileController.promptForAdd(),this.getSelectedRegion());
        });
        removeRegionButton.setOnAction(e ->{
            deleteSelectedRegion(fileController.promptForDelete(),this.getSelectedRegion());
        });
        editRegionButton.setOnAction(e ->{
            enableRegionEditing(true);
        });
        updateRegionButton.setOnAction(e ->{
            updateRegion(fileController.promptForUpdate());
        });
        cancelUpdateButton.setOnAction(e ->{
            cancelUpdate();
        });
    }
    
    /**
     * This method cancels the editing and resets all the information.
     * co-author: Limeng Ruan
     */
    private void cancelUpdate(){
        resetAllInput();
        enableRegionEditing(false);
    }
    
    /**
     * This method changes the information of the selected region according to user input.
     * @param confirm boolean value to determine whether to change it or not.
     * co-author: Limeng Ruan
     */
    private void updateRegion(boolean confirm){
        if((!this.getSelectedRegion().getId().equals(this.getInputId()))&&worldDataManager.hasRegion(new Region(this.getInputId(),this.getInputName(),this.getInputType()))){
            fileController.uniqueIdAlert();
            return;
        }
        if(confirm==true){
            this.getSelectedRegion().setCapital(getInputCapital());
            this.getSelectedRegion().setId(getInputId());
            this.getSelectedRegion().setName(getInputName());
            this.getSelectedRegion().setType(getInputType());
            enableRegionEditing(false);
            fileController.markFileAsNotSaved();
            enableFileControls(true);
            Region thisRegion=this.getSelectedRegion();
            initWorldTree();
            selectRegion(thisRegion);
            
        }
    }
   
    /**
     * This method deletes the region selected. And select a sibling region if there is any, otherwise,
     * select its parent region
     * @param confirm boolean value of whether to proceed the deleting.
     * @param regionToDelete The region to be deleted.
     * co-author: Limeng Ruan
     */
    private void deleteSelectedRegion(boolean confirm, Region regionToDelete){
        if(confirm==true){
            worldDataManager.removeRegion(regionToDelete);
            TreeItem toDelete=worldTree.getSelectionModel().getSelectedItem();
            worldTree.getSelectionModel().getSelectedItem().getParent().getChildren().remove(toDelete);
            if(this.getSelectedRegion().hasSubRegions()){
                selectRegion((Region)this.getSelectedRegion().getSubRegions().next());
            }
            fileController.markFileAsNotSaved();
            enableFileControls(true);
            
        }
    }
    
    /**
     * This method add new region to the file using worldDataManager and add a node to the tree.
     * @param regionID The id returned from promptForAdd()
     * @param parentRegion The selected region, aka the parent of the region to be added.
     * co-author: Limeng Ruan
     */
    private void addRegion(String regionID, Region parentRegion){
        Region regionToAdd=new Region(regionID, regionID, parentRegion.getType());
        if(worldDataManager.hasRegion(regionToAdd)){
            fileController.uniqueIdAlert();
            return;
        }
        worldDataManager.addRegion(regionToAdd,parentRegion);
        worldTree.getSelectionModel().getSelectedItem().getChildren().add(new TreeItem(regionToAdd));
        
        selectRegion(regionToAdd);
        fileController.markFileAsNotSaved();
        enableRegionEditing(true);
    }
 
    /**
     * Initializes the world tree, loading it with the geographic data currently
     * found inside the data manager. Note that this method is only called 
     * once, after either a new world is created or loaded.
     */
    private void initTree()
    {
        // INIT THE TREE
        worldTree = new TreeView();
        
        // MAKE IT SCROLLABLE
        worldTreeScrollPane = new ScrollPane(worldTree);        
    }

    /**
     * Helper method for initRegionControls that creates and returns
     * a new label with custom text and a standard font.
     * 
     * @param text The text to appear inside the label.
     * 
     * @return A constructed label displaying the text argument.
     */
    private Label initPromptLabel(String text)
    {
        Label label = new Label(text);
        label.getStyleClass().add(CLASS_PROMPT_LABEL);
        return label;
    }
  
    /**
     * Helper method for initRegionControls that creates and returns
     * a new text field with a standard number of columns and font. 
     * 
     * @return A constructed text field.
     */
    private TextField initRegionEditorTextField()
    {
        TextField textField = new TextField();
        textField.getStyleClass().add(CLASS_TEXT_FIELD);
        return textField;
    }
    
    /**
     * This method is for enabling or disabling the controls associated
     * with changing region details, meaning the text fields for entering
     * the id, name, etc.
     * 
     * @param enable If true, the controls are enabled, else they
     * are disabled. Note that the rest of the app's controls
     * will all be disabled when we are editing a region.
     */
    public void enableRegionEditing(boolean enable)
    {
        // ACTIVATE/DEACTIVATE ALL FILE CONTROLS
        enableFileControls(!enable);
        
        // ACTIVATE/DEACTIVATE EDIT REGION BUTTON
        enableRegionEditControls(!enable);
        
        // ACTIVATE/DEACTIVATE TREE 
        enableWorldTree(!enable);
        
        // ACTIVATE/DEACTIVATE REGION EDITING CONTROLS
        enableRegionEditor(enable);        
    }

    /**
     * This method can enable and disable the controls used 
     * for changing region data.
     * 
     * @param enabled If true, the region input controls
     * are enabled, else they are disabled.
     */
    public void enableRegionEditor(boolean enabled)
    {
        idTextField.setDisable(!enabled);
        nameTextField.setDisable(!enabled);
        typeComboBox.setDisable(!enabled);
        capitalTextField.setDisable(!enabled);
        updateRegionButton.setDisable(!enabled);
        cancelUpdateButton.setDisable(!enabled);
    }    
    
    /**
     * Used for enabling and disabling the save button. Note
     * that it determines its own state by checking to see if
     * the world has been saved or not since the last edit.
     */
    public void enableSaveButton()
    {
        boolean shouldBeEnabled = !fileController.isSaved();
        saveButton.setDisable(!shouldBeEnabled);
    }

    /**
     * Used for enabling and disabling the save as button
     * 
     * @param enable If true, the save as button is enabled,
     * if false it is disabled.
     */
    public void enableSaveAsButton(boolean enable)
    {
        saveAsButton.setDisable(!enable);
    }
    
    /**
     * Used for enabling and disabling the file controls.
     * 
     * @param enabled If true, the file controls are enabled,
     * if false they are disabled. Note that the save button
     * is still dependent upon if the current world has 
     * already been saved or not.
     */
    public void enableFileControls(boolean enabled)
    {
        newButton.setDisable(!enabled);
        openButton.setDisable(!enabled);
        exitButton.setDisable(!enabled);
        
        if (fileController.isSaved())
        {
            saveButton.setDisable(!false);
            saveAsButton.setDisable(!false);
        }
        else
        {
            saveButton.setDisable(!enabled);
            saveAsButton.setDisable(!enabled);
        }
    }
    
    /**
     * This method enables and disables the buttons used for adding,
     * removing, and activating editing of a region.
     * 
     * @param enabled If true, the buttons are enabled, else they
     * are disabled.
     */
    public void enableRegionEditControls(boolean enabled)
    {
        addRegionButton.setDisable(!enabled);
        removeRegionButton.setDisable(!enabled);
        editRegionButton.setDisable(!enabled);
    }

    /**
     * This method is used for enabling and disable the world tree.
     * 
     * @param enabled If true, the world tree is enabled, allowing
     * the user to interact with it, else it is disabled.
     */
    public void enableWorldTree(boolean enabled)
    {
        worldTree.setDisable(!enabled);
    }
    
    /**
     * This method is used for enabling and disabling the cancel button.
     * 
     * @param enabled If true, the cancel button is enabled, else
     * it is disabled.
     */
    public void enableCancelButton(boolean enabled)
    {
        cancelUpdateButton.setDisable(!enabled);
    }
    
    // BELOW ARE ALL THE METHODS ASSOCIATED WITH
    // DISPLAYING AND MANIPULATING THE TREE
    /**
     * This method reloads all the data inside the world data manager
     * into the tree and makes the selectedRegion argument the selected
     * node, which it opens up the tree to.
     * 
     * @param selectedRegion Node to be selected and displayed after
     * the tree is reloaded.
     */
    public void refreshWorldTree(Region selectedRegion)
    {
        LinkedList<Region> selectionPath = worldDataManager.getPathFromRoot(selectedRegion);
        Region world = worldDataManager.getWorld();
        TreeItem root = new TreeItem(world);
        addSubRegionsToTree(world, root);
        TreeItem<Region> selectedNode = worldTree.getSelectionModel().getSelectedItem();
	selectedNode.setExpanded(true);
    }
    
    /**
     * Initializes the tree with the proper data, including setting
     * up the tree's data model.
     */
    public void initWorldTree()
    {
        Region world = worldDataManager.getWorld();
        worldTreeRoot = new TreeItem(world);
	worldTree.setRoot(worldTreeRoot);
        
        //ADD A LISTENER FOR THE TREE
        worldTree.getSelectionModel().selectedIndexProperty().addListener((v, oldValue, newValue)->{
            RegionClicked(this.getSelectedRegion());
        });
        
        // BUILD THE REST OF THE TREE
        addSubRegionsToTree(world, worldTreeRoot);
        selectRootNode();
	
	// NOW PUT THE WORKBENCH INSIDE THE WINDOW
        windowPane.setCenter(splitPane);
    }  
    
    /**
     * Gets called when a Region(TreeItem) is selected.
     * @param region The region selected.
     */
    public void RegionClicked(Region region){
        displayRegion(region);
        enableRegionEditControls(true);
        enableRegionEditing(false);
        
    }

    /**
     * This helper method adds children to parents and cascades this
     * on down through tree to build it out.
     * 
     * @param regionWithSubRegionsToAdd The region with children to add.
     * 
     * @param parentNode The parent node that we'll add the child nodes to.
     */
    private void addSubRegionsToTree(Region regionWithSubRegionsToAdd, TreeItem parentNode)
    {
        // WE'LL NEED TO ARRANGE THEM IN SORTED ORDER BY NAME
        TreeMap<String, Region> subRegions = new TreeMap();
        Iterator<Region> subRegionsIterator = regionWithSubRegionsToAdd.getSubRegions();
        while (subRegionsIterator.hasNext())
        {
            Region subRegion = subRegionsIterator.next();
            subRegions.put(subRegion.getId(), subRegion);
        }

        // ADD ALL THE CHILDREN
        Iterator<String> keysIterator = subRegions.keySet().iterator();
        while (keysIterator.hasNext())
        {
            String subRegionName = keysIterator.next();
            Region subRegion = subRegions.get(subRegionName);
	    TreeItem subNode = new TreeItem(subRegion);
	    parentNode.getChildren().add(subNode);
            
            // HERE IS OUR RECURSIVE CALL TO CASCADE IT ON DOWN
            addSubRegionsToTree(subRegion, subNode);
        }
    }

    /**
     * Forces the data from the regionToDisplay argument into the
     * region editing controls.
     * 
     * @param regionToDisplay The region the user wishes to view.
     * co-author: Limeng Ruan
     */
    private void displayRegion(Region regionToDisplay)
    {
        // GET ALL THIS REGION'S DATA AND DISPLAY IT
        idTextField.setText(regionToDisplay.getId());
        nameTextField.setText(regionToDisplay.getName());
        typeComboBox.getSelectionModel().select(regionToDisplay.getType());
        if (regionToDisplay.hasCapital())
        {
            capitalTextField.setDisable(false);
            capitalTextField.setText(regionToDisplay.getCapital());
        } else
        {
            capitalTextField.setDisable(true);
            capitalTextField.setText(EMPTY_TEXT);
        }
    }

    /**
     * Gets the region that corresponds to the node that's currently
     * selected.
     * 
     * @return The region selected in the world tree.
     */
    public Region getSelectedRegion()
    {
        // GET THE PATH TO THE NODE
        TreeItem<Region> selectedNode = worldTree.getSelectionModel().getSelectedItem();
        if (selectedNode != null)
        {
            // AND GET THE LAST ONE, WHICH IS THE SELECTED ONE
            return selectedNode.getValue();
        }
        return null;
    }

    /**
     * Gets and returns the tree node that contains the nodeRegion argument.
     * 
     * @param nodeRegion The Region that corresponds to the node we're looking for.
     * 
     * @return The node that contains the nodeRegion as its userObject (i.e. tree node data).
     */
    public TreeItem<Region> getRegionNode(Region nodeRegion)
    {
        // START AT THE ROOT
        TreeItem<Region> walker = worldTreeRoot;
        Region testRegion = walker.getValue();
        if (testRegion.getId().equals(nodeRegion.getId()))
        {
            return walker;
        }
        else
        {
            // THIS WILL BE A RECURSIVE SEARCH METHOD
            return findDescendantNode(walker, nodeRegion);
        }
    }

    /**
     * This recursive helper method cascades the search for a node
     * that stores the region argument as data on down the tree.
     * 
     * @param node The node we examine in looking for region. Note we'll cascade
     * this search recursively on down through node's children.
     * 
     * @param region The region we're searching for.
     * 
     * @return The node that contains region. If not found, null is returned.
     */
    private TreeItem<Region> findDescendantNode(TreeItem<Region> node, Region region)
    {
        // GO THROUGH ALL OF node's CHILDREN
        for (int i = 0; i < node.getChildren().size(); i++)
        {
            TreeItem<Region> childNode = node.getChildren().get(i);
            Region testRegion = childNode.getValue();
            if (testRegion.getId().equals(region.getId()))
            {
                return childNode;
            }
            else if (!childNode.isLeaf())
            {
                // RECURSIVE CALL HERE
                TreeItem<Region> foundNode = findDescendantNode(childNode, region);
                if (foundNode != null)
                {
                    return foundNode;
                }
            }
        }        
        return null;
    }

    /**
     * Gets and returns the parent region of the selected region.
     * 
     * @return The parent region of the selected region.
     */
    public Region getSelectedRegionParent()
    {
	TreeItem<Region> selectedNode = worldTree.getSelectionModel().getSelectedItem();
	if (selectedNode != null)
	    return selectedNode.getParent().getValue();
	else
	    return null;
    }
    
    /**
     * Resets the id text field to the selected region's data.
     */
    public void resetIdInput()
    {
        Region region = getSelectedRegion();
        idTextField.setText(region.getId());
    }

    /**
     * Resets the name text field to the selected region's data.
     */
    public void resetNameInput()
    {
        Region region = getSelectedRegion();
        nameTextField.setText(region.getName());
    }

    /**
     * Resets the type combo box to the selected region's data.
     */
    public void resetTypeInput()
    {
        Region region = getSelectedRegion();
        typeComboBox.getSelectionModel().select(region.getType());
    }

    /**
     * Resets the capital text field to the selected region's data
     */
    public void resetCapitalInput()
    {
        Region region = getSelectedRegion();
        capitalTextField.setText(region.getCapital());
    }

    /**
     * Resets all the region editing text fields to the currently
     * selected region's data.
     */
    public void resetAllInput()
    {
        resetIdInput();
        resetNameInput();
        resetTypeInput();
        resetCapitalInput();
    }
 
    /**
     * Selects the root node in the tree and opens the tree to
     * display its children.
     */    
    public void selectRootNode()
    {
        worldTree.getSelectionModel().select(worldTreeRoot);
    }
    
    /**
     * Selects the node in the tree that contains the regionToSelect
     * data. Note that this method will search through the tree
     * for that data.
     * 
     * @param regionToSelect Region to select in the tree.
     */
    public void selectRegion(Region regionToSelect)
    {
        // ONLY LOOK FOR IT IF IT EXISTS
        if (worldDataManager.hasRegion(regionToSelect))
        {
            LinkedList<Region> pathToRegion = worldDataManager.getPathFromRoot(regionToSelect);
            TreeItem<Region> walker = worldTreeRoot;
            boolean regionFound = false;
            int pathIndex = 0;
            while (!regionFound)
            {
                Region testRegion = walker.getValue();
                if (testRegion.getId().equals(regionToSelect.getId()))
                {
                    // SELECT THIS NODE
                    worldTree.getSelectionModel().select(walker);
		    walker.setExpanded(true);
                    regionFound = true;
                }
                // LOOK THROUGH THE CHILD NODES FOR THE NEXT ONE
                else
                {
                    TreeItem<Region> testNode;
                    boolean childFound = false;
                    for (int i = 0; i < walker.getChildren().size() && !childFound; i++ )
                    {
                        testNode = walker.getChildren().get(i);
                        testRegion = testNode.getValue();
                        Region testChild = pathToRegion.get(pathIndex + 1);
                        if (testRegion.getId().equals(testChild.getId()))
                        {
                            childFound = true;
                            walker = testNode;
                        }
                    }
                    pathIndex++;
                }
            }
        }
    }        
}